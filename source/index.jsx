import React from 'react';
import ReactDOM from 'react-dom';

import { Article, Title } from './elements.jsx'
const fizzBuzz = () => {
  alert('Hello Hacker News')
}
const styles = {
  box: {
    backgroundColor: 'white',
    border: '1px solid #e1e1e1',
    padding: 20,
    borderRadius: 5,
    position: 'absolute',
    width: 200,
    height: 'auto',
    left: '50%',
    marginLeft: -120,
    top: '50%',
    marginTop: -10
  },
  counter: {
    width: '100%',
    color: "white",
    borderWidth: 0,
    padding: '0 0 10px',
    color: '#333',
    textAlign: 'center'
  },
  button: {
    width: '100%',
    backgroundColor: '#42A5F5',
    color: "white",
    borderWidth: 0,
    padding: '10px 0',
    cursor: 'pointer',
    margin: '0 auto'
  },
  articles:{
    borderBottom: '1px solid #eee',
    marginBottom: 10
  }
}

const threeElements = (
  <div style={styles.articles}>
    <Article content="Article 1" />
    <Article content="Article 2" />
    <Article content="Article 3" />
  </div>
)

const myReactEl = (
  <div>
    <Title content="My First Element"/>
    <div className="box" style={styles.box}>
      {threeElements}
      <button style={styles.button} onClick={fizzBuzz}>Click Me</button>
    </div>
  </div>
)

ReactDOM.render(myReactEl, document.getElementById('App'));
