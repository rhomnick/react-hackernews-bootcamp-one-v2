import React from 'react';

const styles = {
  title: {
    width: '100%',
    textAlign: 'center',
    fontFamily: 'Patrick Hand',
    fontSize: 40,
    position: 'absolute',
    width: 500,
    height: 'auto',
    left: '50%',
    marginLeft: -250,
    top: '50%',
    marginTop: -100
  },
  article: {
    padding: '5px 0'
  }
}

export const Title = ({ content }) => {
  return <div style={styles.title}>
    {content}
  </div>
}

export const Article = ({ content }) => {
  return <div style={styles.article}>
    {content}
  </div>
}